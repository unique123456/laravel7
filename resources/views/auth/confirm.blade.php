@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                    <h1 style="text-align:center; margin-top:1rem;">OK?</h1>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ session::get('register')['name'] }}</label>   
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-mail Address') }}</label>
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ session::get('register')['email'] }}</label>
                        </div>
                        

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                    <button type="button" onclick="location.href='{{action('Auth\RegisterController@signup')}}'" class="btn btn-primary">
                                        {{ __('Go Back') }}</button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
